# build-drupal
Build a Drupal 7 site using a build script and custom install profile.

How it works:
-------------
This build script takes your install profile from a GIT repo and creates a Drupal site around it.
* All contrib modules, themes and libraries are placed in `/sites/[site host]/modules`.
* Your profile is stored in `/profiles/[profile name]`. Which is connected to your GIT repo so you can make changes and pull without needing to rebuild.
* All updates can be managed by Drush if required.

Arguments:
----------
* $1 the git repo of the site you're building
* $2 the git branch
* $3 the destination folder where you want the site to be built to
* $4 the site host domain
* $5 the build environment name or install profile name

Example usage:
--------------
```
./build-drupal.sh git@git.plaas.co:cowandsheep.git master cowandsheep.co.za cowandsheep.co.za cowandsheep
```

File management:
----------------
This system does not change the way files are managed in Drupal.

A suggested way of handling that is to mount `/sites/[site host]/files` from a file server like GlusterFS or Amazon S3.
This was files can be managed externally to the website and backed up a as required.

[todo]: Add file management options to the build script.

Structure of a site:
--------------------
See the documentation below for more info on any of the profile files.

```
	my_custom_profile
    ├── libraries
    │   └── custom             # custom libraries go here
    ├── modules
    │   └── custom             # custom modules go here
    ├── themes
    │   └── custom             # custom themes go here
    ├── build-[profile name]
    ├── [profile name].info
    ├── [profile name].install
    ├── [profile name].profile
    └── README.md
```

build-[profile name].make:
--------------------------
This is the initial make file that gets Drupal and your install profile from GIT.

```
core = 7.x
api = 2

; Get Drupal Core.
projects[] = "drupal"

; Install profile
projects[my_custom_profile][type] = "profile"
projects[my_custom_profile][download][type] = "git"
projects[my_custom_profile][download][url] = "git@github.com:awesomesauce/my_custom_profile.git"
```

[profile name].make:
--------------------
This file defines the modules, themes and libraries you want Drush to download.
You can also define where you want the project/library to live.

```
core = 7.x
api = 2

; Modules
projects[admin_menu][subdir] = "contrib"
projects[betterlogin][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[token][subdir] = "contrib"

; Themes
projects[boilerplate][type] = "theme"
projects[boilerplate][subdir] = "contrib"
```

[profile name].info:
--------------------
The info file of your install profile. Define the profile name, description and dependencies.
All dependencies will be installed automatically.

```
name = My Custom Install Profile
description = This is my custom install profile
core = 7.x

; Core
dependencies[] = block
dependencies[] = help
dependencies[] = image
dependencies[] = list
dependencies[] = menu
dependencies[] = number
dependencies[] = options
dependencies[] = path
dependencies[] = taxonomy
dependencies[] = search
dependencies[] = field
dependencies[] = field_ui
dependencies[] = file
dependencies[] = rdf

; Contrib
dependencies[] = admin_menu
dependencies[] = betterlogin
dependencies[] = ctools
dependencies[] = entity
dependencies[] = entity_token
dependencies[] = entityreference
dependencies[] = module_filter
dependencies[] = pathauto
dependencies[] = token

; Custom
dependencies[] = my_custom_module
```

[profile name].install:
-----------------------
Any custom installation instructions.

```php
<?php

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function my_custom_install_profile_install() {
  // Custom stuff.
}

?>
```

[profile name].profile:
-----------------------
Custom profile install instructions.

```php
<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function my_custom_install_profile_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = t('My site');

  $form['server_settings']['site_default_country']['#default_value'] = t('ZA');
}

?>
```

