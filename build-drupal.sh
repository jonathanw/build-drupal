#!/bin/bash

##
# $1 git repo
# $2 the destination folder
# $3 the site host
# $4 the build environment name
#
# Example:
# ./build-drupal.sh git@git.plaas.co:cowandsheep.git cowandsheep.co.za cowandsheep.co.za cowandsheep
##

GIT_REPO=$1
GIT_BRANCH=$2
BUILD_DEST=$3
SITE_HOST=$4
BUILD_ENV=$5
CURR_DIR="$(pwd)"

echo ""
echo ""
echo ":: BUILD DRUPAL SCRIPT ::"
echo ""
echo ""

echo "Removing old cloned repo if exists:"
rm -rf /tmp/$BUILD_DEST

echo ""
echo ""
echo "Clone $GIT_REPO to /tmp/$BUILD_DEST:"
git clone $GIT_REPO /tmp/$BUILD_DEST
cd /tmp/$BUILD_DEST
git checkout $GIT_BRANCH

echo ""
echo ""
echo "Run the platform make file from /tmp/$BUILD_DEST:"
drush make /tmp/$BUILD_DEST/build-$BUILD_ENV.make $BUILD_DEST

echo ""
echo ""
echo "Remove the tmp clone:"
rm -rf /tmp/$BUILD_DEST

echo ""
echo ""
echo "Setup settings.php and the files directory:"
cd $BUILD_DEST
mkdir -p sites/$SITE_HOST/files
cp sites/default/default.settings.php sites/$SITE_HOST/settings.php
chmod -R 777 sites/$SITE_HOST/files sites/$SITE_HOST/settings.php

echo ""
echo ""
echo "Move modules, themes and libraries to sites/$SITE_HOST:"
cd $CURR_DIR/$BUILD_DEST
mv $CURR_DIR/$BUILD_DEST/profiles/$BUILD_ENV/modules $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/.
mv $CURR_DIR/$BUILD_DEST/profiles/$BUILD_ENV/themes $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/.
mv $CURR_DIR/$BUILD_DEST/profiles/$BUILD_ENV/libraries $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/.
rm -rf $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/modules/custom
rm -rf $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/themes/custom

# Symlink the custom modules and themes dir.
ln -s $CURR_DIR/$BUILD_DEST/profiles/$BUILD_ENV/modules/custom $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/modules/custom
ln -s $CURR_DIR/$BUILD_DEST/profiles/$BUILD_ENV/themes/custom $CURR_DIR/$BUILD_DEST/sites/$SITE_HOST/themes/custom

echo ""
echo ""
echo "Clone install profile:"
rm -rf profiles/$BUILD_ENV
cd profiles
git clone $GIT_REPO

echo ""
echo ""
echo "Check the Git repo is working:"
cd $BUILD_ENV
git status
git checkout $GIT_BRANCH

echo ""
echo ""
echo "Done"
echo ""
echo ""

